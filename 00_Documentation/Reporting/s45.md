# Reporting S43-44-45 | Projet Traitement de signal audio embarqué temps-réél sur carte STM32

*Lun. 15/11/21 09:03*

Bonjour,

Ces trois dernières semaines je me suis principalement concentré sur la documentation du dépôt Git. Cette documentation se fait via :
- La création d'un "Readme" pour les différentes parties du projet (Scripts Python de support, Bibliothèques de filtrage, Code embarqué).
Il me reste à affiner le readme général, ainsi que ceux des sources embarquées

- La Rédaction et l'export de la documentation "Doxygen" pour les sources C


J'ai aussi établi un plan pour le rapport :
<ol>
    <li>
        Besoin / Attentes du Projet
    </li>
    <li>
        Plateforme Cible
        <ol>
            <li>Presentation STM32</li>
            <li>Chaine de Capture (général) (du son vers l'échantillon)</li>
            <li>Sortie Audio</li>
        </ol>
    </li>
    <li>Conversion PDM -> PCM
        <ol>
            <li>Les Echantillons
                <ul>
                    <li>PCM ?</li>
                    <li>PDM ?</li>
                </ul>
            <li>Chaine de Filtrage</li>
            <li>Intégration sur le µ-contrôleur
        </ol>    
    </li>
    <li>
        Les Démonstrateurs
        <ol>
            <li>Présentation des démonstrateurs
                <ul>
                    <li>"Parrot"</li>
                    <li>"Digital Recorder"</li>
                    <li>"DIrect Output"</li>
                </ul>
            </li>
            <li>Utilisation des peripheriques</li>
        </ol>
    </li>
</ol>

Cordialement,

**Pierre-Yves JÉZÉGOU**

FIPA 2021
