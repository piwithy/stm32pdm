<h1> Reporting </h1>

Ce dossier contien une copies des emails de reporting
 - [Semaine 38 (22&rarr;24/09/2021)](s38.md) : Découverte du Projet
 - [Semaine 39 (27/09&rarr;01/10/2021)](s39.md) : Acquisition Filtrage
 - [Semaine 40 (04&rarr;08/10/2021)](s40.md) : Premier démonstrateur
 - [Semaine 41 (11&rarr;15/10/2021)](s41.md) : Enregisteur USB
 - [Semaine 42 (18&rarr;22/10/2021)](s42.md) : Connexion à un casque
 - [Semaine 43 (25&rarr;29/10/2021)](s43.md) : Documentation
 - [Semaine 44 (01&rarr;05/11/2021)](s44.md) : Documentation
 - [Semaine 45 (08&rarr;12/11/2021)](s45.md) : Documentation
 - [Semaine 46 (15&rarr;19/11/2021)](s46.md) : Rapport
 - [Semaine 47 (22&rarr;26/11/2021)](s47.md) : Rapport
